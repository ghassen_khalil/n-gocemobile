package com.formation.Testexpandable;

public class Sol {
	String id;
	String Nom;
	String val;
	
	public Sol(String id, String nom, String val) {
		super();
		this.id = id;
		Nom = nom;
		this.val = val;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	

}
