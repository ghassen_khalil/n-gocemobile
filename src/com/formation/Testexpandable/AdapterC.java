package com.formation.Testexpandable;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterC extends BaseAdapter {

	private ArrayList<Cj> client,client1;;
	private LayoutInflater myInflater;
	
	public AdapterC (Context context, ArrayList<Cj> _produits)
	{	this.client1=_produits;
		this.myInflater = LayoutInflater.from(context);
		this.client = _produits;
	}
	
	public int getCount() {
		return this.client.size();
	}

	public Object getItem(int arg0) {
		return this.client.get(arg0);
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView date;
		TextView id;
		TextView lib;
		TextView val;
		
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null)
		{
			convertView = myInflater.inflate(R.layout.cj_row, null);
			holder = new ViewHolder();
			holder.date = (TextView) convertView.findViewById(R.id.cjdate);
			holder.id = (TextView) convertView.findViewById(R.id.cjid);
			holder.lib = (TextView) convertView.findViewById(R.id.cjnom);
			holder.val = (TextView) convertView.findViewById(R.id.cjval);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.date.setText(client.get(position).getDate());
		holder.id.setText("Id :"+client.get(position).getId());
		holder.lib.setText("   Lib : "+client.get(position).getLib());
		holder.val.setText("Valeur: "+client.get(position).getVal()+" .dt");
	

		return convertView;
		
	}

 
	public Filter getFilter() {
		 
		 return new Filter() {
		
		 
		 @SuppressWarnings("unchecked")
		 
		 @Override
		 
		 protected void publishResults(CharSequence constraint, FilterResults results) {
			
			 Log.i("Amine","publish result");
			 
			 client = (ArrayList<Cj>) results.values;
		 AdapterC.this.notifyDataSetChanged();
		 
		 }
		 
		 @Override
		 
		 protected FilterResults performFiltering(CharSequence constraint) {
			 Log.i("Amine","performfiltring");
	
		 ArrayList<Cj> filteredResults = getFilteredResults(constraint);
		 
		 FilterResults results = new FilterResults();
		 
		 results.values = filteredResults;
		 
		 return results; 
		 }
		 
		 private ArrayList<Cj> getFilteredResults(CharSequence constraint) {
			 Log.i("Amine","get filtred result");
		 ArrayList<Cj> filteredTeams = new ArrayList<Cj>();
		 Log.i("Amine",constraint.toString());
		 Log.i("Amine","size clinet"+client.size());
		 Log.i("Amine","size clinet1"+client1.size());
		 for(int i=0;i< client1.size();i++){
		 
		 if(client1.get(i).getLib().toLowerCase().contains(constraint.toString().toLowerCase())){
			 
		 filteredTeams.add(client1.get(i));
		 
		 }
		 
		 }
		 
		 return filteredTeams;
		 
		 }
		 
		 };
		 
		 }
}