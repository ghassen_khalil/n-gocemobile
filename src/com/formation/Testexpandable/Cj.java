package com.formation.Testexpandable;

public class Cj {
	String date;
	String id ;
	String Lib;
	String val;
	
	public Cj(String date, String id, String lib, String val) {
		super();
		this.date = date;
		this.id = id;
		Lib = lib;
		this.val = val;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLib() {
		return Lib;
	}
	public void setLib(String lib) {
		Lib = lib;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	
}
