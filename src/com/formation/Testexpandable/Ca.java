package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.formation.Testexpandable.R.drawable;


public class Ca extends Activity{
	private GraphicalView mChartView;
	TextView T ;
	 String Pseudo;
     String Type1;
     String Titre;
     String per;
     String form;
     String strURL;
 	InputStream is = null;
	String result=null;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
		setContentView(R.layout.ca);
		T=(TextView) findViewById(R.id.titstat);
		T.setText("Statistiques");
		final Bundle extras = getIntent().getExtras();
		 Pseudo = extras.getString("Pseudo");
	     Type1 = extras.getString("Type");
	     Titre=extras.getString("Titre");
	     per=extras.getString("per");
	     form=extras.getString("form");
	     Log.i("per", per);
	     Log.i("per", form);
	     
	     String x="0" ;
	     if (Titre.contains("Frs")){
	    	 x="1";
	     }
	     
	     
	     // Journalier
	     
	   if (per.endsWith("journalier")){
		final ArrayList<Cj> Cjs = new ArrayList <Cj> ();
		strURL = "http://www.net-deco.net/ca.php";
	
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type",Type1));
		nameValuePairs.add(new BasicNameValuePair("pseudo",Pseudo));
		nameValuePairs.add(new BasicNameValuePair("tit",x));

		// Envoie de la commande http
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection " + e.toString());
		
		}

		// Convertion de la requête en string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				
			}
			is.close();
			result=sb.toString();
			
		}catch(Exception e){
			Log.e("log_tag", "Error converting result " + e.toString());
		
		}
		if(per.contains("journalier"))
		{
			// Parse les données JSON
			try{
				JSONArray jArray = new JSONArray(result);
				for(int i=0;i<jArray.length();i++){
					JSONObject json_data = jArray.getJSONObject(i);
					Cjs.add(new Cj(json_data.getString("date"),json_data.getString("id"),json_data.getString("R_S"),json_data.getString("val")));
					}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
			// Cat�gorie *****************************
			
			
			
			CategorySeries Series =new CategorySeries("");
			int[] colors=new int[Cjs.size()];
			for (int i=0;i<Cjs.size() ;i++)
			{
				int color;
			    Random rnd = new Random(); 
			    color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
			    colors[i]=color;
				Series.add(Cjs.get(i).getLib().toString()+": "+Cjs.get(i).getVal().toString() ,Double.parseDouble(Cjs.get(i).getVal().toString()));
			}
			XYMultipleSeriesDataset Dataset=new XYMultipleSeriesDataset(); 
		      Dataset.addSeries(Series.toXYSeries()) ;
		      //Pie Code *****************************
		      if (form.equals("pie")){
		  		
		    		
		    	     
	      			
		      	   
		        	DefaultRenderer render=new DefaultRenderer();
		        	for (int color:colors)
		        	{
		        		SimpleSeriesRenderer r=new SimpleSeriesRenderer();
		        		r.setColor(color);
		        		render.addSeriesRenderer(r);
		        	} 
		        	 LinearLayout layout = (LinearLayout) findViewById(R.id.chart1);
		        	 if (mChartView == null) {
		        			  mChartView= ChartFactory.getPieChartView(this, Series, render);
		        			  layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		        	    		  }
		        		   else {
		        				 layout.removeView(mChartView);
		        				 mChartView= ChartFactory.getPieChartView(this, Series, render);
		        			 layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
		        			     } 
		  		}
		  		//Barchart code*********************
		  		
		  		
		  		
		  		
		  		
		  		
		  		else {
		  			
		  		      XYSeriesRenderer renderer=new XYSeriesRenderer();
		  		      renderer.setColor(Color.CYAN);
		  		        renderer.setDisplayChartValues(true);
		  		        renderer.setChartValuesSpacing((float)1);
		  		         XYMultipleSeriesRenderer mRenderer =new XYMultipleSeriesRenderer();
		  		         mRenderer.addSeriesRenderer(renderer);
		  		         mRenderer.setXTitle("Tiers");
		  		         mRenderer.setYTitle("Valeur");
		  		     	 LinearLayout layout = (LinearLayout) findViewById(R.id.chart1);
		  		      	 if (mChartView == null) {
		  		      			  mChartView= ChartFactory.getBarChartView(this, Dataset, mRenderer, Type.DEFAULT);
		  		      			  layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		  		      	    		  }
		  		      		   else {
		  		      				 layout.removeView(mChartView);
		  		      				 mChartView= ChartFactory.getBarChartView(this, Dataset, mRenderer, Type.DEFAULT );
		  		      			 layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
		  		      			     } 
		  		    }
		}
		
		
		
		
		
		
	   }
		
		
		
		
		
		// Annuel --------------------------------------
		if(per.contains("Annuel"))
		{
			strURL ="http://www.net-deco.net/caa.php";
			
			// Envoie de la commande http
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(strURL);
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection " + e.toString());
			
			}

			// Convertion de la requête en string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
					
				}
				is.close();
				result=sb.toString();
				
			}catch(Exception e){
				Log.e("log_tag", "Error converting result " + e.toString());
			
			}
			Log.i("per", "Annuel");
			String a[] = {"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Nouvembre","Decembre"};
			String b[]= {"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Nouvembre","Decembre"};
			int sss=0;
			// Parse les données JSON
			try{
				JSONArray jArray = new JSONArray(result);
				for(int i=0;i<jArray.length();i++){
					JSONObject json_data = jArray.getJSONObject(i);
					a[i]= json_data.getString("valeur");
					Log.i("Amine", a[i]);
					Log.i("Amine", b[i]);
					sss++;
					}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
			// Cat�gorie *****************************
			
			
			
						CategorySeries Series =new CategorySeries("");
						int[] colors=new int[sss];
						for (int i=0;i<sss;i++)
						{
							int color;
						    Random rnd = new Random(); 
						    color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
						    colors[i]=color;
							Series.add(b[i]+": "+a[i] ,Double.parseDouble(a[i]));
						}
						XYMultipleSeriesDataset Dataset=new XYMultipleSeriesDataset(); 
					      Dataset.addSeries(Series.toXYSeries()) ;
					      
					      
					      //Pie Code *****************************
					      if (form.equals("pie")){
					  		
					    		
					    	     
				      			
					      	   
					        	DefaultRenderer render=new DefaultRenderer();
					        	for (int color:colors)
					        	{
					        		SimpleSeriesRenderer r=new SimpleSeriesRenderer();
					        		r.setColor(color);
					        		render.addSeriesRenderer(r);
					        	} 
					        	 LinearLayout layout = (LinearLayout) findViewById(R.id.chart1);
					        	 if (mChartView == null) {
					        			  mChartView= ChartFactory.getPieChartView(this, Series, render);
					        			  layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					        	    		  }
					        		   else {
					        				 layout.removeView(mChartView);
					        				 mChartView= ChartFactory.getPieChartView(this, Series, render);
					        			 layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
					        			     } 
					  		}
					  		//Barchart code*********************
					  		
					  		
					  		
					  		
					  		
					  		
					  		else {
					  			
					  		      XYSeriesRenderer renderer=new XYSeriesRenderer();
					  		      renderer.setColor(Color.CYAN);
					  		        renderer.setDisplayChartValues(true);
					  		        renderer.setChartValuesSpacing((float)1);
					  		         XYMultipleSeriesRenderer mRenderer =new XYMultipleSeriesRenderer();
					  		         mRenderer.addSeriesRenderer(renderer);
					  		         mRenderer.setXTitle("Tiers");
					  		         mRenderer.setYTitle("Valeur");
					  		     	 LinearLayout layout = (LinearLayout) findViewById(R.id.chart1);
					  		      	 if (mChartView == null) {
					  		      			  mChartView= ChartFactory.getBarChartView(this, Dataset, mRenderer, Type.DEFAULT);
					  		      			  layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					  		      	    		  }
					  		      		   else {
					  		      				 layout.removeView(mChartView);
					  		      				 mChartView= ChartFactory.getBarChartView(this, Dataset, mRenderer, Type.DEFAULT );
					  		      			 layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
					  		      			     } 
					  		    }
					
			
		
		}
		
	      
	      
	      
		
		
	   
	}
	public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(0,1000,0,"Journalier").setIcon(android.R.drawable.ic_menu_month);
    	menu.add(0, 1001, 0, "Mensuel").setIcon(android.R.drawable.ic_menu_my_calendar);
    	menu.add(0,1002,0,"Annuel").setIcon(android.R.drawable.ic_menu_agenda);
    	menu.add(0,1003,0,"Bar Graph").setIcon(drawable.chart1);
    	menu.add(0,1004,0,"Camembert Graph").setIcon(drawable.pie_chart1);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()){
    	
    	case 1000:
    		Intent intent = new Intent();
            
        intent.setClass(this, Ca.class);
        intent.putExtra("per", "journalier");
        intent.putExtra("Pseudo", Pseudo);
			intent.putExtra("Type", Type1);
			intent.putExtra("Titre",Titre);
			intent.putExtra("form",form);
        startActivity(intent);
        this.finish();
        break;
    	case 1001:
    		Intent intent1 = new Intent();
            
            intent1.setClass(this, Ca.class);
            intent1.putExtra("per", "Mensuel");
            intent1.putExtra("Pseudo", Pseudo);
    			intent1.putExtra("Type", Type1);
    			intent1.putExtra("Titre",Titre);
    			intent1.putExtra("form",form);
            startActivity(intent1);
            this.finish();
            break;
    	case 1002:
    		Intent intent11 = new Intent();
            
            intent11.setClass(this, Ca.class);
            intent11.putExtra("per", "Annuel");
                intent11.putExtra("Pseudo", Pseudo);
    			intent11.putExtra("Type", Type1);
    			intent11.putExtra("Titre",Titre);
    			intent11.putExtra("form",form);
            startActivity(intent11);
            this.finish();
            break;
    	case 1003:
    		Intent intent111 = new Intent();
            
            intent111.setClass(this, Ca.class);
            intent111.putExtra("per", per);
                intent111.putExtra("Pseudo", Pseudo);
    			intent111.putExtra("Type", Type1);
    			intent111.putExtra("Titre",Titre);
    			intent111.putExtra("form","bar");
            startActivity(intent111);
            this.finish();
            break;
    	case 1004:
    		Intent intent1111 = new Intent();
            
            intent1111.setClass(this, Ca.class);
            intent1111.putExtra("per", per);
                intent1111.putExtra("Pseudo", Pseudo);
    			intent1111.putExtra("Type", Type1);
    			intent1111.putExtra("Titre",Titre);
    			intent1111.putExtra("form","pie");
            startActivity(intent1111);
            this.finish();
            break;
    	}
    	return super.onOptionsItemSelected(item);
    }	
}