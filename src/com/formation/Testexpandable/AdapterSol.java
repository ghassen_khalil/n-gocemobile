package com.formation.Testexpandable;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterSol extends BaseAdapter {

	private ArrayList<Sol> client;
	private LayoutInflater myInflater;
	
	public AdapterSol (Context context, ArrayList<Sol> _produits)
	{	
		this.myInflater = LayoutInflater.from(context);
		this.client = _produits;
	}
	
	public int getCount() {
		return this.client.size();
	}

	public Object getItem(int arg0) {
		return this.client.get(arg0);
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView date;
		TextView id;
		TextView lib;
		TextView val;
		
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null)
		{
			convertView = myInflater.inflate(R.layout.sol_row, null);
			holder = new ViewHolder();
			holder.id = (TextView) convertView.findViewById(R.id.cjid);
			holder.lib = (TextView) convertView.findViewById(R.id.cjnom);
			holder.val = (TextView) convertView.findViewById(R.id.cjval);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		holder.id.setText(client.get(position).getId());
		holder.lib.setText(client.get(position).getNom());
		holder.val.setText(client.get(position).getVal()+".000 Dt");
	

		return convertView;
		
	}

}
