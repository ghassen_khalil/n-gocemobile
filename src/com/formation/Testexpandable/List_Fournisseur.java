package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class List_Fournisseur extends Activity {
	ListView L ;
	EditText T;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    final Bundle extras = getIntent().getExtras();
	    final String Pseudo = extras.getString("Pseudo");
	    final String Type = extras.getString("Type");
		setContentView(R.layout.list_fournisseur);
		final ArrayList<Client> Clients = new ArrayList <Client> ();
		final String strURL = "http://www.net-deco.net/list_fournisseur.php";
		InputStream is = null;
		String result=null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type",Type));
		nameValuePairs.add(new BasicNameValuePair("pseudo",Pseudo));


			// Envoie de la commande http
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(strURL);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection " + e.toString());
			
			}

			// Convertion de la requête en string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
					
				}
				is.close();
				result=sb.toString();
				
			}catch(Exception e){
				Log.e("log_tag", "Error converting result " + e.toString());
			
			}
			// Parse les données JSON
			try{
				JSONArray jArray = new JSONArray(result);
				for(int i=0;i<jArray.length();i++){
					JSONObject json_data = jArray.getJSONObject(i);
					Clients.add(new Client(json_data.getString("Code"),json_data.getString("R_S"),json_data.getString("C_TVA"),json_data.getString("Adr"),json_data.getString("tel"),json_data.getString("fax")));
					}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
						
			
	
		


		L = (ListView) findViewById(R.id.listf);
		final Adapter adapter =new Adapter(this,Clients);
		L.setAdapter(adapter);
		T=(EditText)findViewById(R.id.txtrecherchef);
		T.clearFocus();
		TextWatcher filterTextWatcher = new TextWatcher() {

		    public void afterTextChanged(Editable s) {
		    }

		    public void beforeTextChanged(CharSequence s, int start, int count,
		            int after) {
		    }

		    public void onTextChanged(CharSequence s, int start, int before,
		            int count) {
		    	 adapter.getFilter().filter(s.toString()); 
		    }

		};
		T.addTextChangedListener(filterTextWatcher);
		L.setTextFilterEnabled(true);
		//On instancie notre layout en tant que View
        LayoutInflater factory = LayoutInflater.from(this);
        final View alertDialogView = factory.inflate(R.layout.alterdialogperso, null);
 
        //Cr�ation de l'AlertDialog
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
 
        //On affecte la vue personnalis� que l'on a cr�e � notre AlertDialog
        adb.setView(alertDialogView);
 
     
 
        //On cr�e un bouton "Annuler" � notre AlertDialog et on lui affecte un �v�nement
        adb.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	//Lorsque l'on cliquera sur annuler on quittera l'application
            	finish();
          } });
       
	
		L.setOnItemLongClickListener(new OnItemLongClickListener(){
			public boolean onItemLongClick(AdapterView<?> adapter, View arg1,
					int arg2, long arg3) {
				final Object B =adapter.getItemAtPosition(arg2);
				final Client c = (Client) B; 
				adb.setTitle("Le Fournisseur : "+c.getNom().toString());
				final CharSequence[] items = {"Appeler", "Modifier", "Supprimer"};
				adb.setItems(items, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int item) {
				        if (item ==1){
							Intent I = new Intent (List_Fournisseur.this,Modifier_Client.class);
							I.putExtra("Pseudo",Pseudo);
							I.putExtra("Type",Type);
							I.putExtra("Type1", "Frs.");
							I.putExtra("Num", c.getNum());
							I.putExtra("Nom", c.getNom());
							I.putExtra("Tva", c.getTva());
							I.putExtra("Adr", c.getAdr());
							I.putExtra("Tel", c.getTel());
							I.putExtra("Mail",c.getMail());
							startActivity(I);
							finish();
				        }
				        if (item==0){
				        	String url = "tel:"+c.getTel().toString();
				        	Log.i("Amine", url);
				        	Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
				        	startActivity(intent);
				        	finish();
				        }
				        if (item==2){
				        	ArrayList<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
				        	nameValuePairs1.add(new BasicNameValuePair("num",c.getNum()));
				        	final String strURL1 = "http://www.net-deco.net/supp_tiers.php";
				        	InputStream is1 = null;
				        	String result1=null;
				        	try{
								HttpClient httpclient = new DefaultHttpClient();
								HttpPost httppost = new HttpPost(strURL1);
								httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
								HttpResponse response = httpclient.execute(httppost);
								HttpEntity entity = response.getEntity();
								is1 = entity.getContent();
				        	}catch(Exception e){
								Log.e("log_tag", "Error in http connection " + e.toString());
							
							}

							// Convertion de la requête en string
							try{
								BufferedReader reader = new BufferedReader(new InputStreamReader(is1,"iso-8859-1"),8);
								StringBuilder sb = new StringBuilder();
								String line = null;
								while ((line = reader.readLine()) != null) {
									sb.append(line);
									
								}
								is1.close();
								result1=sb.toString();
							}catch(Exception e){
								Log.e("log_tag", "Error converting result " + e.toString());
							
							}
								
				    		if (result1.contains("Delete")){
				    			Toast t ;
								t = Toast.makeText(List_Fournisseur.this.getApplicationContext(), "Enregistement supprim�", Toast.LENGTH_LONG);
								t.show();
								List_Fournisseur.this.finish();					
				    		}
				    		else {
				    			Toast t ;
								t = Toast.makeText(List_Fournisseur.this.getApplicationContext(), "Erreur R�seau", Toast.LENGTH_LONG);
								t.show();
				    		}
				        }
				        	
				    }
				});
       
				 adb.show();

				return false;
			}
			
		});
		
	}

}
