package com.formation.Testexpandable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class SuivieActivity extends Activity {
	ImageButton stk;
	ImageButton BCC;
	ImageButton BCF;
	ImageButton BEC;
	ImageButton BEF;

	ProgressDialog 	dialog;
	Thread			thread;
	Handler 		handler;
	 public void onCreate(Bundle savedInstanceState)
	 {
	
     super.onCreate(savedInstanceState);
	 setContentView(R.layout.suivie_layout);
	 final Bundle extras = getIntent().getExtras();
     final String Pseudo = extras.getString("Pseudo");
     final String Type = extras.getString("Type");
     TextView T = (TextView) findViewById(R.id.PPPPPP);
     T.setText("Utilisateur:   "+extras.get("Pseudo").toString());
	 stk=(ImageButton)findViewById(R.id.IBStock); 
	 BCC=(ImageButton)findViewById(R.id.BCC); 
	 BCF=(ImageButton)findViewById(R.id.BSF);
	 BEC=(ImageButton)findViewById(R.id.EchC); 
	 BEF=(ImageButton)findViewById(R.id.EchF); 
	 
	 dialog=new ProgressDialog(this);
	 stk.setOnClickListener(new OnClickListener (){
		
		public void onClick(View v) {	
			 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	 	        dialog.setMessage("Chargement...");
	 	        dialog.show();        
	 	         
	 	        handler = new Handler(){
	 	    		public void handleMessage(Message msg) { 
	 	    			switch(msg.what) {
	 	    			
	 	    			case 1:
	 	    				dialog.dismiss();
	 	    				break;
	 	    			   	
	 	    			}
	 	    		}
	 	        };
	 	        thread = new Thread(){
	 	        	public void run(){
	 	        		
	 	    						try {
	 									thread.sleep(1000);
	 								} catch (InterruptedException e) {
	 									// TODO Auto-generated catch block
	 									e.printStackTrace();
	 								}
	 	    		
	 	    			handler.sendEmptyMessage(1);    			
	 	        	};
	 	        };
	 	        thread.start();
			Intent I = new Intent(SuivieActivity.this, Stock.class);
			startActivity (I);
		}
		 
	 });
	 BCF.setOnClickListener(new OnClickListener (){
			
			public void onClick(View v) {	
				 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		 	        dialog.setMessage("Chargement...");
		 	        dialog.show();        
		 	         
		 	        handler = new Handler(){
		 	    		public void handleMessage(Message msg) { 
		 	    			switch(msg.what) {
		 	    			
		 	    			case 1:
		 	    				dialog.dismiss();
		 	    				break;
		 	    			   	
		 	    			}
		 	    		}
		 	        };
		 	        thread = new Thread(){
		 	        	public void run(){
		 	        		
		 	    						try {
		 									thread.sleep(1000);
		 								} catch (InterruptedException e) {
		 									// TODO Auto-generated catch block
		 									e.printStackTrace();
		 								}
		 	    		
		 	    			handler.sendEmptyMessage(1);    			
		 	        	};
		 	        };
		 	        thread.start();
				Intent I = new Intent(SuivieActivity.this, Solde.class);
				I.putExtra("Pseudo", Pseudo);
				I.putExtra("Type", Type);
				I.putExtra("Titre","Client");
				startActivity (I);
			}
			 
		 });
	 BCC.setOnClickListener(new OnClickListener (){
			
			public void onClick(View v) {	
				 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		 	        dialog.setMessage("Chargement...");
		 	        dialog.show();        
		 	         
		 	        handler = new Handler(){
		 	    		public void handleMessage(Message msg) { 
		 	    			switch(msg.what) {
		 	    			
		 	    			case 1:
		 	    				dialog.dismiss();
		 	    				break;
		 	    			   	
		 	    			}
		 	    		}
		 	        };
		 	        thread = new Thread(){
		 	        	public void run(){
		 	        		
		 	    						try {
		 									thread.sleep(1000);
		 								} catch (InterruptedException e) {
		 									// TODO Auto-generated catch block
		 									e.printStackTrace();
		 								}
		 	    		
		 	    			handler.sendEmptyMessage(1);    			
		 	        	};
		 	        };
		 	        thread.start();
				Intent I = new Intent(SuivieActivity.this, Solde.class);
				I.putExtra("Pseudo", Pseudo);
				I.putExtra("Type", Type);
				I.putExtra("Titre","Frs");
				startActivity (I);
			}
			 
		 });
	 BEC.setOnClickListener(new OnClickListener (){
			
			public void onClick(View v) {	
				 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		 	        dialog.setMessage("Chargement...");
		 	        dialog.show();        
		 	         
		 	        handler = new Handler(){
		 	    		public void handleMessage(Message msg) { 
		 	    			switch(msg.what) {
		 	    			
		 	    			case 1:
		 	    				dialog.dismiss();
		 	    				break;
		 	    			   	
		 	    			}
		 	    		}
		 	        };
		 	        thread = new Thread(){
		 	        	public void run(){
		 	        		
		 	    						try {
		 									thread.sleep(1000);
		 								} catch (InterruptedException e) {
		 									// TODO Auto-generated catch block
		 									e.printStackTrace();
		 								}
		 	    		
		 	    			handler.sendEmptyMessage(1);    			
		 	        	};
		 	        };
		 	        thread.start();
				Intent I = new Intent(SuivieActivity.this, Echan.class);
				I.putExtra("Pseudo", Pseudo);
				I.putExtra("Type", Type);
				Log.i("Amine", Type);
				I.putExtra("Titre","Echeancier Client");
				startActivity (I);
			}
			 
		 });
	 BEF.setOnClickListener(new OnClickListener (){
			
			public void onClick(View v) {	
				 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		 	        dialog.setMessage("Chargement...");
		 	        dialog.show();        
		 	         
		 	        handler = new Handler(){
		 	    		public void handleMessage(Message msg) { 
		 	    			switch(msg.what) {
		 	    			
		 	    			case 1:
		 	    				dialog.dismiss();
		 	    				break;
		 	    			   	
		 	    			}
		 	    		}
		 	        };
		 	        thread = new Thread(){
		 	        	public void run(){
		 	        		
		 	    						try {
		 									thread.sleep(1000);
		 								} catch (InterruptedException e) {
		 									// TODO Auto-generated catch block
		 									e.printStackTrace();
		 								}
		 	    		
		 	    			handler.sendEmptyMessage(1);    			
		 	        	};
		 	        };
		 	        thread.start();
				Intent I = new Intent(SuivieActivity.this, Echan.class);
				I.putExtra("Pseudo", Pseudo);
				I.putExtra("Type", Type);
				Log.i("Amine", Type);
				I.putExtra("Titre","Echeancier Fournisseur");
				startActivity (I);
			}
			 
		 });
	 }

}
