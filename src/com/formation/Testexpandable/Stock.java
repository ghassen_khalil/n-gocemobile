package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

public class Stock extends Activity {
	ListView L ; 
	EditText T;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stock);   
		     
		final ArrayList<Produit> Produits = new ArrayList <Produit> ();
		final String strURL = "http://www.net-deco.net/stock.php";
		InputStream is = null;
		String result=null;
	

			// Envoie de la commande http
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(strURL);
			
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection " + e.toString());
			
			}

			// Convertion de la requête en string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
					
				}
				is.close();
				result=sb.toString();
				
			}catch(Exception e){
				Log.e("log_tag", "Error converting result " + e.toString());
			
			}
			// Parse les données JSON
			try{
				JSONArray jArray = new JSONArray(result);
				for(int i=0;i<jArray.length();i++){
					JSONObject json_data = jArray.getJSONObject(i);
					Produits.add(new Produit(json_data.getString("IDProd"),json_data.getString("CodProd"),json_data.getString("LibProd"),json_data.getString("QtStk"),json_data.getString("PrixVente"),json_data.getString("nom")));
					}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
			L=(ListView)findViewById(R.id.listpro);
			final AdapterS adapter =new AdapterS(this,Produits);
			L.setAdapter(adapter);
			T=(EditText)findViewById(R.id.txtfilter);
			T.clearFocus();
			TextWatcher filterTextWatcher = new TextWatcher() {

			    public void afterTextChanged(Editable s) {
			    }

			    public void beforeTextChanged(CharSequence s, int start, int count,
			            int after) {
			    }

			    public void onTextChanged(CharSequence s, int start, int before,
			            int count) {
			    	 adapter.getFilter().filter(s.toString()); 
			    }

			};
			T.addTextChangedListener(filterTextWatcher);
	}
}
