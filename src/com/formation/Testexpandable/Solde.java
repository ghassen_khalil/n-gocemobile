package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

public class Solde extends Activity{
	ListView L;
	TextView tit;

	@Override
	public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
		setContentView(R.layout.solde);
		final Bundle extras = getIntent().getExtras();
		final String Pseudo = extras.getString("Pseudo");
	     final String Type = extras.getString("Type");
	     final String Titre=extras.getString("Titre");
	 	tit=(TextView)findViewById(R.id.TitreS);
	 	tit.setText("Solde Client");
		 String x="0" ;
	     if (Titre.contains("Frs")){
	    	 x="1";
	    	 tit.setText("Solde fournisseur");
	     }
	 
	   
		L=(ListView) findViewById(R.id.L_S);
		final ArrayList<Sol> Sols = new ArrayList <Sol> ();
		final String strURL = "http://www.net-deco.net/solde.php";
		InputStream is = null;
		String result=null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type",Type));
		nameValuePairs.add(new BasicNameValuePair("pseudo",Pseudo));
		nameValuePairs.add(new BasicNameValuePair("tit",x));

		// Envoie de la commande http
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection " + e.toString());
		
		}

		// Convertion de la requête en string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				
			}
			is.close();
			result=sb.toString();
			
		}catch(Exception e){
			Log.e("log_tag", "Error converting result " + e.toString());
		
		}
		// Parse les données JSON
		try{
			JSONArray jArray = new JSONArray(result);
			for(int i=0;i<jArray.length();i++){
				JSONObject json_data = jArray.getJSONObject(i);
				Sols.add(new Sol(json_data.getString("id"),json_data.getString("R_S"),json_data.getString("val")));
				}
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
		final AdapterSol adapter =new AdapterSol(this,Sols);
		L.setAdapter(adapter);
	}
}
