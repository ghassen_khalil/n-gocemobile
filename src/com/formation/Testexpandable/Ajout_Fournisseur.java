package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Ajout_Fournisseur  extends Activity {
	ImageView enr ;
	ImageView eff ;
	EditText nom ;
	EditText adr ;
	EditText tel;
	EditText fax;
	EditText tva;
	TextView T;
	public static final String strURL = "http://www.net-deco.net/ajout_fournisseur.php";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 final Bundle extras = getIntent().getExtras();
	     final String Pseudo = extras.getString("Pseudo");
		setContentView(R.layout.form_client);
		enr=(ImageView)findViewById(R.id.addClt);
		eff=(ImageView)findViewById(R.id.viderchamp);
		nom=(EditText)findViewById(R.id.txtNom);
		adr=(EditText)findViewById(R.id.txtAdress);
		tel=(EditText)findViewById(R.id.txtTel);
		fax=(EditText)findViewById(R.id.txtfax);
		tva=(EditText)findViewById(R.id.txtTva);
		nom.clearFocus();
		adr.clearFocus();
		tel.clearFocus();
		fax.clearFocus();
		tva.clearFocus();
		T=(TextView)findViewById(R.id.titre1);
		T.setText("Nouveau Frns.");
		eff.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				nom.setText("");
				adr.setText("");
				tel.setText("");
				fax.setText("");	
				tva.setText("");
				
			}
			});
		enr.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
			InputStream is = null;
			String result = null;
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("nom",nom.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair ("tva",tva.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair ("adr",adr.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair ("tel",tel.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair ("fax",fax.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair ("pseudo",Pseudo));
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(strURL);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection " + e.toString());
			}

			// Convertion de la requête en string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				is.close();
				result=sb.toString();
				
			}catch(Exception e){
				Log.e("log_tag", "Error converting result " + e.toString());
			}
			if(result.contains("Ok")){
				Toast t ;
				t = Toast.makeText(Ajout_Fournisseur.this.getApplicationContext(), "Fournisseur Ajout�", Toast.LENGTH_LONG);
				t.show();
				Ajout_Fournisseur.this.finish();					
}
			else 
			{Toast t ;
			t = Toast.makeText(Ajout_Fournisseur.this.getApplicationContext(), "Erreur Connection", Toast.LENGTH_LONG);
			t.show();
			}
			}
			});
	}
}