package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class Login extends Activity {
	ImageButton B;
	EditText Pseudo;
	EditText Password;
	TextView INF;
	ProgressDialog 	dialog;
	Thread			thread;
	Handler 		handler;
	public static final String strURL = "http://www.net-deco.net/login.php";
	public boolean isOnline() {
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo ni = cm.getActiveNetworkInfo();
	    if (ni!=null && ni.isAvailable() && ni.isConnected()) {
	        return true;
	    } else {
	        return false; 
	    }
	}
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		//On instancie notre layout en tant que View
        LayoutInflater factory = LayoutInflater.from(this);
		final View alertDialogView = factory.inflate(R.layout.loading, null);
		 
        //Cr�ation de l'AlertDialog
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
 
        //On affecte la vue personnalis� que l'on a cr�e � notre AlertDialog
        adb.setView(alertDialogView);
 
	
		Pseudo = (EditText) findViewById(R.id.Pseudo);
		Password = (EditText) findViewById(R.id.Password);
		B = (ImageButton) findViewById(R.id.b);
		INF=(TextView) findViewById(R.id.inf);
		if (isOnline())
		{
			Password.setEnabled(true);
			Pseudo.setEnabled(true);
			B.setEnabled(true);
			INF.setText("Etat :Connect�");
			INF.setBackgroundColor(Color.GREEN);
		}
		else {
			Password.setEnabled(false);
			Pseudo.setEnabled(false);
			B.setEnabled(false);
			INF.setText("Etat : Verifiez vos service de donn�es ou connectez votre Wifi");
			INF.setBackgroundColor(Color.RED);
		}
		dialog=new ProgressDialog(this);
			B.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			        dialog.setMessage("Chargement...");
			        dialog.show();        
			         
			        handler = new Handler(){
			    		public void handleMessage(Message msg) { 
			    			switch(msg.what) {
			    			
			    			case 1:
			    				dialog.dismiss();
			    				break;
			    			   	
			    			}
			    		}
			        };
			        thread = new Thread(){
			        	public void run(){
			        		
			    						try {
											Thread.sleep(1000);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} 		
			    			handler.sendEmptyMessage(1);    			
			        	};
			        };
			        thread.start();
					InputStream is = null; 
					String result = null;
			// Envoyer la requête au script PHP.
						// Script PHP : $sql=mysql_query("select * from tblVille where Nom_ville like '".$_REQUEST['ville']."%'");
						// $_REQUEST['ville'] sera remplacé par L dans notre exemple.
						// Ce qui veut dire que la requête enverra les villes commençant par la lettre L
						ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("Pseudo",Pseudo.getText().toString()));
						nameValuePairs.add(new BasicNameValuePair ("Password",Password.getText().toString()));

						// Envoie de la commande http
						try{
							HttpClient httpclient = new DefaultHttpClient();
							HttpPost httppost = new HttpPost(strURL);
							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
							HttpResponse response = httpclient.execute(httppost);
							HttpEntity entity = response.getEntity();
							is = entity.getContent();

						}catch(Exception e){
							Log.e("log_tag", "Error in http connection " + e.toString());
							INF.setText(e.toString());
						}

						// Convertion de la requête en string
						try{
							BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
							StringBuilder sb = new StringBuilder();
							String line = null;
							while ((line = reader.readLine()) != null) {
								sb.append(line);
							}
							is.close();
							result=sb.toString();
							
						}catch(Exception e){
							Log.e("log_tag", "Error converting result " + e.toString());
							INF.setText(e.toString());
						}
						try{
							if(result.contains("Chef") || result.contains("Agent")){
							JSONArray jArray = new JSONArray(result);
								JSONObject json_data = jArray.getJSONObject(0);
								Intent I = new Intent(Login.this, TestexpandableActivity.class);
								I.putExtra("Type", json_data.getString("type"));
								I.putExtra("Pseudo",json_data.getString("Pseudo"));
								startActivity(I);
								finish();
								}
							else 
							{
								
							INF.setBackgroundColor(Color.RED);
							INF.setText("Verifier Votre Login et mot de passe")	;
							}
						}catch(JSONException e){
							Log.e("log_tag", "Error parsing data " + e.toString());
						}
						
						
							
							
							
							
							
							
}
						
				
					}
			);
			
		
	}
	public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(0, 1001, 0, "Quitter").setIcon(android.R.drawable.ic_lock_power_off);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()){
    	case 1001:
    		this.finish();
    	}
    	return super.onOptionsItemSelected(item);
    }
	}

