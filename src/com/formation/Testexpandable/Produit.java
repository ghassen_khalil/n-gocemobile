package com.formation.Testexpandable;

public class Produit {
	String id ;
	String code;
	String lib;
	String qte;
	String prix;
	String famille;
	public Produit(String id, String code, String lib, String qte, String prix , String famille) {
		super();
		this.id = id;
		this.code = code;
		this.lib = lib;
		this.qte = qte;
		this.prix = prix;
		this.famille=famille;
	}
	public String getFamille() {
		return famille;
	}
	public void setFamille(String famille) {
		this.famille = famille;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public String getQte() {
		return qte;
	}
	public void setQte(String qte) {
		this.qte = qte;
	}
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	
}