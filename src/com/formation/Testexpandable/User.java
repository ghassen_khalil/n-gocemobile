package com.formation.Testexpandable;

public class User {
	String name ;
	String Type ;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public User(String name, String type) {
		super();
		this.name = name;
		Type = type;
	}
	
}
