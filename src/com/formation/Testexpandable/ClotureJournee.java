package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ClotureJournee extends Activity{
	private GraphicalView mChartView;
	TextView T;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	     super.onCreate(savedInstanceState);
		setContentView(R.layout.ca);
		T= (TextView) findViewById(R.id.titstat);
		T.setText("Cloture de la journ�e");
		final Bundle extras = getIntent().getExtras();
		final String Pseudo = extras.getString("Pseudo");
	     final String Type = extras.getString("Type");
	     final String Titre=extras.getString("Titre");
	     String x="0" ;
	     if (Titre.contains("Frs")){
	    	 x="1";
	     }
	     Log.i("Amine P",Pseudo);
	     Log.i("Amine T",Type);
	     Log.i("Amine Tit",Titre);
		final ArrayList<Cj> Cjs = new ArrayList <Cj> ();
		final String strURL = "http://www.net-deco.net/ca.php";
		InputStream is = null;
		String result=null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type",Type));
		nameValuePairs.add(new BasicNameValuePair("pseudo",Pseudo));
		nameValuePairs.add(new BasicNameValuePair("tit",x));

		// Envoie de la commande http
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection " + e.toString());
		
		}

		// Convertion de la requête en string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				
			}
			is.close();
			result=sb.toString();
			
		}catch(Exception e){
			Log.e("log_tag", "Error converting result " + e.toString());
		
		}
		// Parse les données JSON
		try{
			JSONArray jArray = new JSONArray(result);
			for(int i=0;i<jArray.length();i++){
				JSONObject json_data = jArray.getJSONObject(i);
				Cjs.add(new Cj(json_data.getString("date"),json_data.getString("id"),json_data.getString("R_S"),json_data.getString("val")));
				}
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
		CategorySeries Series =new CategorySeries("0000");
		int[] colors=new int[Cjs.size()];
		for (int i=0;i<Cjs.size() ;i++)
		{
			int color;
		    Random rnd = new Random(); 
		    color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
		    colors[i]=color;
			Series.add(Cjs.get(i).getLib().toString(),Double.parseDouble(Cjs.get(i).getVal().toString()));
		}
	
      	//{0xffff0600,0xff175100,0xff175110,0xff175100,0xff175120,0xff175130};
      			
      	   XYMultipleSeriesDataset Dataset=new XYMultipleSeriesDataset(); 
      	      Dataset.addSeries(Series.toXYSeries()) ;
      	DefaultRenderer render=new DefaultRenderer();
      	for (int color:colors)
      	{
      		SimpleSeriesRenderer r=new SimpleSeriesRenderer();
      		r.setColor(color);
      		render.addSeriesRenderer(r);
      	} 
      	 LinearLayout layout = (LinearLayout) findViewById(R.id.chart1);
      	 if (mChartView == null) {
      			  mChartView= ChartFactory.getPieChartView(this, Series, render);
      			  layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
      	    		  }
      		   else {
      				 layout.removeView(mChartView);
      				 mChartView= ChartFactory.getPieChartView(this, Series, render);
      			 layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
      		   }
	}
}
