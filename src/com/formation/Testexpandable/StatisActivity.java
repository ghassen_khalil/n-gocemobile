package com.formation.Testexpandable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class StatisActivity extends Activity {
	ImageButton chart ; 
	ImageButton cloture;
	ProgressDialog 	dialog;
	Thread			thread;
	Handler 		handler;
	 public void onCreate(Bundle savedInstanceState)
	 {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.statis_layout);
     chart=(ImageButton)findViewById(R.id.chart);
     cloture=(ImageButton)findViewById(R.id.cloture);
     final Bundle extras = getIntent().getExtras();
     final String Pseudo = extras.getString("Pseudo");
     final String Type = extras.getString("Type");
     TextView T = (TextView) findViewById(R.id.PPPPPP);
     T.setText("Utilisateur:   "+extras.get("Pseudo").toString());
	 //TextView textview = new TextView(this);
     //textview.setText("Statistiques");
     //setContentView(textview);
     chart.setOnClickListener(new OnClickListener(){

 		public void onClick(View arg0) {
 			Intent I = new Intent(StatisActivity.this, Ca.class);
 			I.putExtra("Pseudo", Pseudo);
			I.putExtra("Type", Type);
			I.putExtra("Titre","Frs");
			I.putExtra("per", "journalier");
 			I.putExtra("form", "pie");
 			startActivity (I);
 		}
     	
     });dialog=new ProgressDialog(this);
     cloture.setOnClickListener(new OnClickListener(){

  		public void onClick(View arg0) {
  			Intent I = new Intent(StatisActivity.this, Ca.class);
  			I.putExtra("Pseudo", Pseudo);
 			I.putExtra("Type", Type);
 			I.putExtra("Titre","Frs");
 			I.putExtra("per", "journalier");
 			I.putExtra("form", "pie");
  			startActivity (I);
  		}
      	
      });dialog=new ProgressDialog(this);
 	 
	 }
	

}
