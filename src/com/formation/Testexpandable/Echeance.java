package com.formation.Testexpandable;

public class Echeance {
String Date;
String Id;
String nom;
String mnt;
String mnt_ver;
String mnt_nec;
String ver;
String date_ver;
String type;
public Echeance(String date, String id, String nom, String mnt, String mnt_ver,
		String mnt_nec, String ver, String date_ver, String type) {
	super();
	Date = date;
	Id = id;
	this.nom = nom;
	this.mnt = mnt;
	this.mnt_ver = mnt_ver;
	this.mnt_nec = mnt_nec;
	this.ver = ver;
	this.date_ver = date_ver;
	this.type = type;
}
public String getDate() {
	return Date;
}
public void setDate(String date) {
	Date = date;
}
public String getId() {
	return Id;
}
public void setId(String id) {
	Id = id;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getMnt() {
	return mnt;
}
public void setMnt(String mnt) {
	this.mnt = mnt;
}
public String getMnt_ver() {
	return mnt_ver;
}
public void setMnt_ver(String mnt_ver) {
	this.mnt_ver = mnt_ver;
}
public String getMnt_nec() {
	return mnt_nec;
}
public void setMnt_nec(String mnt_nec) {
	this.mnt_nec = mnt_nec;
}
public String getVer() {
	return ver;
}
public void setVer(String ver) {
	this.ver = ver;
}
public String getDate_ver() {
	return date_ver;
}
public void setDate_ver(String date_ver) {
	this.date_ver = date_ver;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}


}
