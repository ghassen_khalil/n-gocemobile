package com.formation.Testexpandable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class GestClientActivity extends Activity {
	ImageButton Ajout ; 
ImageButton Con;
ProgressDialog 	dialog;
Thread			thread;
Handler 		handler;
	 public void onCreate(Bundle savedInstanceState)
	 
	 {

     super.onCreate(savedInstanceState);
     setContentView(R.layout.clients_layout);
     final Bundle extras = getIntent().getExtras();
     final String Pseudo = extras.getString("Pseudo");
     final String Type = extras.getString("Type");
     Log.i("login", Type);
     TextView T = (TextView) findViewById(R.id.PPPPPP);
     T.setText("Utilisateur:   "+extras.get("Pseudo").toString());
     
     Ajout=(ImageButton)findViewById(R.id.imageButton1);
     Con=(ImageButton)findViewById(R.id.imageButton2);
     
    Ajout.setOnClickListener(new OnClickListener(){

		public void onClick(View arg0) {
			Intent I = new Intent(GestClientActivity.this, Ajout_Client.class);
			
			
			I.putExtra("Pseudo",Pseudo);
			startActivity (I);
		}
    	
    });dialog=new ProgressDialog(this);
    Con.setOnClickListener(new OnClickListener(){

		public void onClick(View arg0) {
			
	        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        dialog.setMessage("Chargement...");
	        dialog.show();        
	         
	        handler = new Handler(){
	    		public void handleMessage(Message msg) { 
	    			switch(msg.what) {
	    			
	    			case 1:
	    				dialog.dismiss();
	    				break;
	    			   	
	    			}
	    		}
	        };
	        thread = new Thread(){
	        	public void run(){
	        		
	    						try {
									thread.sleep(1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	    			
	    	
	    		


	    		
	        		
	    			handler.sendEmptyMessage(1);    			
	        	};
	        };
	        thread.start();
			Intent I = new Intent(GestClientActivity.this, List_Client.class);
			I.putExtra("Type", Type);
			I.putExtra("Pseudo",Pseudo);
			startActivity (I);
			
			
			
		}
    	
    });
    
	 }	

	
}
