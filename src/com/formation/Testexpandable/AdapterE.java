package com.formation.Testexpandable;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterE extends BaseAdapter {

	private ArrayList<Echeance> client;
	private LayoutInflater myInflater;
	
	public AdapterE (Context context, ArrayList<Echeance> _produits)
	{	
		this.myInflater = LayoutInflater.from(context);
		this.client = _produits;
	}
	
	public int getCount() {
		return this.client.size();
	}

	public Object getItem(int arg0) {
		return this.client.get(arg0);
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView date;
		TextView id;
		TextView lib;
		TextView val;
		TextView mnt_verse,mnt_nechu,echu,type,ver;
		
		
		
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null)
		{
			convertView = myInflater.inflate(R.layout.ech_row, null);
			holder = new ViewHolder();
			holder.date = (TextView) convertView.findViewById(R.id.Ech_Date);
			holder.id = (TextView) convertView.findViewById(R.id.Echid);
			holder.lib = (TextView) convertView.findViewById(R.id.Echnom);
			holder.val = (TextView) convertView.findViewById(R.id.Echmnt);
			holder.mnt_nechu=(TextView) convertView.findViewById(R.id.Echmntnech);
			holder.mnt_verse = (TextView) convertView.findViewById(R.id.Echmntver);
			holder.type = (TextView) convertView.findViewById(R.id.Echtype);
			holder.ver=(TextView) convertView.findViewById(R.id.Echver);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.date.setText("Date Ech:"+client.get(position).getDate());
		holder.id.setText("Id :"+client.get(position).getId());
		holder.lib.setText(client.get(position).getNom());
		holder.val.setText("Valeur: "+client.get(position).getMnt()+".000dt");
		holder.mnt_nechu.setText(("Non Echu : "+client.get(position).getMnt_nec()+" .dt"));
		holder.mnt_verse.setText(("Mnt Vers� : "+client.get(position).getMnt_ver()+" .dt"));
		holder.type.setText(("Type : "+client.get(position).getType()));
		holder.ver.setText(("Vers� : "+client.get(position).getVer()));
		

		return convertView;
		
	}

 

}