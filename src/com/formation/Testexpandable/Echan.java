package com.formation.Testexpandable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

public class Echan  extends Activity{
	ListView L ;
	TextView T;
	DatePicker D;
	Button B;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.echeance);
		final Bundle extras = getIntent().getExtras();
		final String Pseudo = extras.getString("Pseudo");
	     final String Type = extras.getString("Type");
	     final String Titre=extras.getString("Titre");
	     L=(ListView)findViewById(R.id.listechean);
	     T=(TextView)findViewById(R.id.titrechean);
	     D=(DatePicker)findViewById(R.id.dateechean);
	     B=(Button)findViewById(R.id.go);
	     T.setText(Titre);
	     String x="1" ;
	     if (Titre.contains("Client")){
	    	 x="0";
	     }
	  
	    final ArrayList<Echeance> Es = new ArrayList <Echeance> ();
	    final ArrayList<Echeance> Ef = new ArrayList <Echeance> ();
	
		final String strURL = "http://www.net-deco.net/ech.php";
		InputStream is = null;
		String result=null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type",Type));
		nameValuePairs.add(new BasicNameValuePair("pseudo",Pseudo));
		nameValuePairs.add(new BasicNameValuePair("tit",x));

		// Envoie de la commande http
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection " + e.toString());
		
		}

		// Convertion de la requête en string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				
			}
			is.close();
			result=sb.toString();
			
		}catch(Exception e){
			Log.e("log_tag", "Error converting result " + e.toString());
		
		}
		// Parse les données JSON
		try{
			JSONArray jArray = new JSONArray(result);
			for(int i=0;i<jArray.length();i++){
				JSONObject json_data = jArray.getJSONObject(i);
				Es.add(new Echeance(json_data.getString("date"),json_data.getString("id"),json_data.getString("R_S"),json_data.getString("mnt"),json_data.getString("mnt_ver"),json_data.getString("mnt_nec"),json_data.getString("ver"),json_data.getString("date_ver"),json_data.getString("type")));
				}
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data " + e.toString());
		}
		int day= D.getDayOfMonth();
	    int month=D.getMonth()+1;
	    int year= D.getYear();
	    String SSS = year+"-"+month+"-"+day;
	   
	    if (month<10){
	    	SSS=year+"-0"+month+"-"+day;
	    }
	    if (day<10){
	    	SSS=year+"-"+month+"-0"+day;
	    }
	    if ((month<10)&&(day<10)){
			   SSS=year+"-0"+month+"-0"+day;
			    }
		for (int i=0;i<Es.size();i++){
			Log.i("SSS",SSS);
			if (Es.get(i).getDate().toString().contains(SSS)){
				
				Ef.add(i, Es.get(i));}
		}
		final AdapterE adapter =new AdapterE(this,Ef);
		L.setAdapter(adapter);
	
	    B.setOnClickListener(new OnClickListener(){

				

			

				public void onClick(View arg0) {
					 
					int day= D.getDayOfMonth();
					    int month=D.getMonth()+1;
					    int year= D.getYear();
					    String SSS = year+"-"+month+"-"+day;
					   
					    if (month<10){
					    	SSS=year+"-0"+month+"-"+day;
					    }
					    if (day<10){
					    	SSS=year+"-"+month+"-0"+day;
					    }
					    if ((month<10)&&(day<10)){
							   SSS=year+"-0"+month+"-0"+day;
							    }
					for (int i=0;i<Es.size();i++){
						Log.i("SSS",SSS);
						if (Es.get(i).getDate().toString().contains(SSS)){
							
							Ef.add(i, Es.get(i));}
					}
							AdapterE adapter =new AdapterE(Echan.this,Ef);
							L.setAdapter(adapter);
							adapter.notifyDataSetChanged();
					
				}
		    	
		    });
		
	
	
	}

}
