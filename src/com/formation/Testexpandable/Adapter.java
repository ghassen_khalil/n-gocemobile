package com.formation.Testexpandable;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class Adapter extends BaseAdapter {

	private ArrayList<Client> client,client1;;
	private LayoutInflater myInflater;
	
	public Adapter (Context context, ArrayList<Client> _produits)
	{	this.client1=_produits;
		this.myInflater = LayoutInflater.from(context);
		this.client = _produits;
	}
	
	public int getCount() {
		return this.client.size();
	}

	public Object getItem(int arg0) {
		return this.client.get(arg0);
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView num;
		TextView nom;
		TextView tva;
		TextView adr;
		TextView tel;
		TextView mail;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null)
		{
			convertView = myInflater.inflate(R.layout.client_row, null);
			holder = new ViewHolder();
			holder.num = (TextView) convertView.findViewById(R.id.adnum);
			holder.nom = (TextView) convertView.findViewById(R.id.adnom);
			holder.tva = (TextView) convertView.findViewById(R.id.adtva);
			holder.adr = (TextView) convertView.findViewById(R.id.adadr);
			holder.tel = (TextView) convertView.findViewById(R.id.adtel);
			holder.mail = (TextView) convertView.findViewById(R.id.admail);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.num.setText(client.get(position).getNum()+" :  ");
		holder.nom.setText(client.get(position).getNom());
		holder.tva.setText("TVA: "+client.get(position).getTva());
		holder.adr.setText("Adr: "+client.get(position).getAdr());
		holder.tel.setText("Tel: "+client.get(position).getTel());
		holder.mail.setText("Fax: "+client.get(position).getMail());

		return convertView;
		
	}

 
	public Filter getFilter() {
		 
		 return new Filter() {
		
		 
		 @SuppressWarnings("unchecked")
		 
		 @Override
		 
		 protected void publishResults(CharSequence constraint, FilterResults results) {
			
			 Log.i("Amine","publish result");
			 
			 client = (ArrayList<Client>) results.values;
		 Adapter.this.notifyDataSetChanged();
		 
		 }
		 
		 @Override
		 
		 protected FilterResults performFiltering(CharSequence constraint) {
			 Log.i("Amine","performfiltring");
	
		 ArrayList<Client> filteredResults = getFilteredResults(constraint);
		 
		 FilterResults results = new FilterResults();
		 
		 results.values = filteredResults;
		 
		 return results; 
		 }
		 
		 private ArrayList<Client> getFilteredResults(CharSequence constraint) {
			 Log.i("Amine","get filtred result");
		 ArrayList<Client> filteredTeams = new ArrayList<Client>();
		 Log.i("Amine",constraint.toString());
		 Log.i("Amine","size clinet"+client.size());
		 Log.i("Amine","size clinet1"+client1.size());
		 for(int i=0;i< client1.size();i++){
		 
		 if(client1.get(i).getNom().toLowerCase().contains(constraint.toString().toLowerCase())){
			 
		 filteredTeams.add(client1.get(i));
		 
		 }
		 
		 }
		 
		 return filteredTeams;
		 
		 }
		 
		 };
		 
		 }
}