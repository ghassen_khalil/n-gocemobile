package com.formation.Testexpandable;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterS extends BaseAdapter {

	private ArrayList<Produit> produit,produit1;
	private LayoutInflater myInflater;
	
	public AdapterS (Context context, ArrayList<Produit> _produits)
	{
		this.myInflater = LayoutInflater.from(context);
		this.produit = _produits;
		this.produit1=_produits;
	}
	
	public int getCount() {
		return this.produit.size();
	}

	public Object getItem(int position) {
		return this.produit.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		TextView num;
		TextView nom;
		TextView qte;
		TextView prix;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		if((position==0)||position>0&&!(produit.get(position).getFamille().toString().equals(produit.get(position-1).getFamille().toString())))
		{
			View v = myInflater.inflate(R.layout.produit_row, null);
			TextView separator=(TextView)v.findViewById(R.id.separator);
			separator.setVisibility(View.VISIBLE);
			separator.setText(produit.get(position).getFamille());
			
			TextView item=(TextView)v.findViewById(R.id.pnum);
        	item.setVisibility(View.VISIBLE);
        	item.setText(produit.get(position).getId()+" : ");
        	TextView item1=(TextView)v.findViewById(R.id.pnom);
        	item1.setVisibility(View.VISIBLE);
        	item1.setText(produit.get(position).getLib());
        	TextView item2=(TextView)v.findViewById(R.id.pqte);
        	item2.setVisibility(View.VISIBLE);
        	item2.setText("Quantit� : "+produit.get(position).getQte());
        	TextView item3=(TextView)v.findViewById(R.id.pprix);
        	item3.setVisibility(View.VISIBLE);
        	item3.setText("Prix : "+produit.get(position).getPrix()+" Dt.");
        	
			
        	return v;
		}
		else
		{
			View v = myInflater.inflate(R.layout.produit_row, null);
			
			TextView item=(TextView)v.findViewById(R.id.pnum);
        	item.setVisibility(View.VISIBLE);
        	item.setText(produit.get(position).getId()+" : ");
        	TextView item1=(TextView)v.findViewById(R.id.pnom);
        	item1.setVisibility(View.VISIBLE);
        	item1.setText(produit.get(position).getLib());
        	TextView item2=(TextView)v.findViewById(R.id.pqte);
        	item2.setVisibility(View.VISIBLE);
        	item2.setText("Quantit� : "+produit.get(position).getQte());
        	TextView item3=(TextView)v.findViewById(R.id.pprix);
        	item3.setVisibility(View.VISIBLE);
        	item3.setText("Prix : "+produit.get(position).getPrix()+" Dt.");
        	
			return v;
		}
	}

 
	public Filter getFilter() {
		 
		 return new Filter() {
		 
		 
		 @SuppressWarnings("unchecked")
		 
		 @Override
		 
		 protected void publishResults(CharSequence constraint, FilterResults results) {
		 
		 produit = (ArrayList<Produit>) results.values;
		 
		 AdapterS.this.notifyDataSetChanged();
		 
		 }
		 
		 @Override
		 
		 protected FilterResults performFiltering(CharSequence constraint) {
		 
		 ArrayList<Produit> filteredResults = getFilteredResults(constraint);
		 
		 FilterResults results = new FilterResults();
		 
		 results.values = filteredResults;
		 
		 return results;
		 
		 }
		 
		 private ArrayList<Produit> getFilteredResults(CharSequence constraint) {
		 
		 ArrayList<Produit> filteredTeams = new ArrayList<Produit>();
		 
		 for(int i=0;i< produit1.size();i++){
		 
		 if(produit1.get(i).getLib().toLowerCase().startsWith(constraint.toString().toLowerCase())){
		 
		 filteredTeams.add(produit1.get(i));
		 
		 }
		 
		 }
		 
		 return filteredTeams;
		 
		 }
		 
		 };
		 
		 }
}