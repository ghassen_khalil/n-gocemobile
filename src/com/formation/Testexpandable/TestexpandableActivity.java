package com.formation.Testexpandable;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

public class TestexpandableActivity extends TabActivity {
    /** Called when the activity is first created. */
	Intent I ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page1);
        final Bundle extras = getIntent().getExtras();
        String Pseudo = extras.getString("Pseudo");
        String Type = extras.getString("Type");
        
        
        Resources res = getResources(); // Resource object to get Drawables
        TabHost tabHost = getTabHost();  // The activity TabHost
        TabHost.TabSpec spec;  // Resusable TabSpec for each tab
        Intent intent;  // Reusable Intent for each tab 

        // Create an Intent to launch an Activity for the tab (to be reused)
        intent = new Intent();
        intent.putExtra("Type", Type);
		intent.putExtra("Pseudo",Pseudo);
		intent.setClass(this, GestClientActivity.class);
        // Initialize a TabSpec for each tab and add it to the TabHost
        spec = tabHost.newTabSpec("Clients").setIndicator("Clients",
                          res.getDrawable(R.drawable.ic_tab_clients))
                      .setContent(intent);
        tabHost.addTab(spec);
        
        
        

        // Do the same for the other tabs
        intent = new Intent();
        intent.putExtra("Type", Type);
		intent.putExtra("Pseudo",Pseudo);
		intent.setClass(this, GestFrsActivity.class);
        spec = tabHost.newTabSpec("Fournisseurs").setIndicator("Fournisseurs",
                          res.getDrawable(R.drawable.ic_tab_frs))
                      .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent();
        intent.putExtra("Type", Type);
		intent.putExtra("Pseudo",Pseudo);
        intent.setClass(this, SuivieActivity.class);
        spec = tabHost.newTabSpec("Suivie").setIndicator("Suivie",
                          res.getDrawable(R.drawable.ic_tab_suivie))
                      .setContent(intent);
        tabHost.addTab(spec);
        if (extras.get("Type").toString().contains("Chef"))
        { 
        	intent = new Intent();
            intent.putExtra("Type", Type);
    		intent.putExtra("Pseudo",Pseudo);
        intent.setClass(this, StatisActivity.class);
        spec = tabHost.newTabSpec("Statistiques").setIndicator("Statistiques",
                          res.getDrawable(R.drawable.ic_tab_stats))
                      .setContent(intent);
        tabHost.addTab(spec);
        }

        tabHost.setCurrentTab(0); 
    }
    public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(0,1000,0,"Se déconnecter").setIcon(android.R.drawable.ic_delete);
    	menu.add(0, 1001, 0, "Quitter").setIcon(android.R.drawable.ic_lock_power_off);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()){
    	
    	case 1000:
    		Intent intent = new Intent();
            
        intent.setClass(this, Login.class);
        startActivity(intent);
        this.finish();
    	case 1001:
    		this.finish();
    	}
    	return super.onOptionsItemSelected(item);
    }
}