package com.formation.Testexpandable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class GestFrsActivity extends Activity {
	protected static final Context Context = null;
	ImageButton Ajout ; 
	ImageButton Con;
	ProgressDialog 	dialog;
	Thread			thread;
	Handler 		handler;
	 public void onCreate(Bundle savedInstanceState)
	 {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.frs_layout);
     final Bundle extras = getIntent().getExtras();
     final String Pseudo = extras.getString("Pseudo");
     final String Type = extras.getString("Type");
     TextView T = (TextView) findViewById(R.id.PPPPPP);
     T.setText("Utilisateur:   "+extras.get("Pseudo").toString());
     Ajout=(ImageButton)findViewById(R.id.imageButton3);
     Con=(ImageButton)findViewById(R.id.imageButton4);
	 //TextView textview = new TextView(this);
     //textview.setText("Gestion Des Fournisseurs");
     //setContentView(textview);
     Ajout.setOnClickListener(new OnClickListener(){

 		public void onClick(View arg0) {
 			Intent I = new Intent(GestFrsActivity.this, Ajout_Fournisseur.class);
 			I.putExtra("Pseudo",Pseudo);
 			startActivity (I);
 			
 		}
     	
     });
     dialog=new ProgressDialog(this);
     Con.setOnClickListener(new OnClickListener(){

 		public void onClick(View arg0) {
 			 dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
 	        dialog.setMessage("Chargement...");
 	        dialog.show();        
 	         
 	        handler = new Handler(){
 	    		public void handleMessage(Message msg) { 
 	    			switch(msg.what) {
 	    			
 	    			case 1:
 	    				dialog.dismiss();
 	    				break;
 	    			   	
 	    			}
 	    		}
 	        };
 	        thread = new Thread(){
 	        	public void run(){
 	        		
 	    						try {
 									thread.sleep(1000);
 								} catch (InterruptedException e) {
 									// TODO Auto-generated catch block
 									e.printStackTrace();
 								}
 	    		
 	    			handler.sendEmptyMessage(1);    			
 	        	};
 	        };
 	        thread.start();
 			
 	 			Intent I = new Intent(GestFrsActivity.this, List_Fournisseur.class);
 	 			I.putExtra("Type", Type);
 				I.putExtra("Pseudo",Pseudo);
 			startActivity (I);
 			
 		}
     	
     });
	 }

}
