package com.formation.Testexpandable;

public class Client {
	private String num ; 
	private String nom;
	private String tva;
	private String adr ;
	private String tel ; 
	private String mail ;
	public Client(String num, String nom, String tva, String adr, String tel,
			String mail) {
		super();
		this.num = num;
		this.nom = nom;
		this.tva = tva;
		this.adr = adr;
		this.tel = tel;
		this.mail = mail;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}
	public String getAdr() {
		return adr;
	}
	public void setAdr(String adr) {
		this.adr = adr;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	

}
